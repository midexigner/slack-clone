import React, { useEffect, useState } from 'react'
import "./Sidebar.css"
import EditIcon from '@material-ui/icons/Edit';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import SidebarOption from '../SidebarOption/SidebarOption';
import InsertCommentIcon from '@material-ui/icons/InsertComment';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import AppsIcon from '@material-ui/icons/Apps';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import db from '../../firebase';
import { useStateValue } from '../../StateProvider';

const Sidebar = ()=> {
    const [channels, setChannels] = useState([]);
    const [{user}] = useStateValue();

    useEffect(() => {
        //run this code ONCE when the sidebar component loads 
        db.collection('rooms').onSnapshot(snapshot => (
            setChannels(
                snapshot.docs.map((doc)=>({
                id : doc.id,
                name : doc.data().name,
            }))
            )
            )
        );
    }, []);
    return (
        <div className="sidebar">
           <div className="sidebar__header">
              <div className="sidebar__info">
              <h2>Slack Clone</h2>
              <h3><FiberManualRecordIcon/> {user?.displayName}</h3>
              </div>
              <EditIcon/>
           </div>
           <SidebarOption Icon={InsertCommentIcon} title="Thread"/>
           <SidebarOption Icon={InboxIcon} title="Mentions & reactions"/>
           <SidebarOption Icon={DraftsIcon} title="Saved items"/>
           <SidebarOption Icon={BookmarkIcon} title="Channel browser"/>
           <SidebarOption Icon={PeopleAltIcon} title="People & user groups"/>
           <SidebarOption Icon={AppsIcon} title="Apps"/>
           <SidebarOption Icon={FileCopyIcon} title="File browser"/>
           <SidebarOption Icon={ExpandLessIcon} title="Show less"/>
           <hr/>
           <SidebarOption Icon={ExpandMoreIcon} title="Channels"/>
           <hr/>
           <SidebarOption Icon={AddIcon} addChannelOption title="Add Channel"/>
        {/* Connect to DB and  */}
        {channels.map(channel => (
            <SidebarOption key={channel.id} title={channel.name} id={channel.id}/>
        ))}
          
        </div>
    )
}

export default Sidebar
