import React from 'react'
import "./Header.css"
import { Avatar } from "@material-ui/core"
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import SearchIcon from '@material-ui/icons/Search';
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined';
import { useStateValue } from '../../StateProvider';
import {auth} from '../../firebase'
import firebase from 'firebase';
const Header = ()=> {
    const [{user}] =useStateValue();
    const signOut = async () => {
        try {
          if (auth) {
            await firebase.auth().signOut();
            alert("Successfully signed out!");
          }
        } catch (error) {
          console.log("error", error);
        }
    };
    return (
        <div className="header">
         <div className="header__left">
         <Avatar 
         className="header__avatar" 
         alt={user?.displayname}
         src={user?.photoURL}
         onClick={()=> signOut()}
         />   
        <AccessTimeIcon/>  
         </div>
         <div className="header__search">
           <SearchIcon/>
           <input 
           placeholder="Search MI Dexigner"
           type="search" 
           className="" />
         </div>
         <div className="header__right">
             <HelpOutlineOutlinedIcon/>
         </div>
        </div>
    )
}

export default Header
