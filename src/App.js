import React, {useEffect } from 'react'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import './App.css';
import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';
import Chat from './pages/Chat/Chat';
import Login from './pages/Login/Login';
import { useStateValue } from './StateProvider';
import {auth} from './firebase'
import { actionTypes } from './reducer';
function App() {
  // const [user, setUser] = useState(null)
  const [{user}, dispatch] = useStateValue();

useEffect(() => {
  const unsubscribe = auth.onAuthStateChanged(user => {
    dispatch({
      type: actionTypes.SET_USER,
      user:user
  })
  })

  return unsubscribe
}, [])
  return (
    <div className="app">
      <Router>
      {!user ? (
     <Login />
     ):(
     <>
     <Header/>
     <div className="app__body">
     {/* sidebar */}
     <Sidebar />
     <Switch>
          <Route path = "/room/:roomId">
          <Chat/>
          </Route>
          <Route path = "/">
            <h1>Welcome</h1>
          </Route>
        </Switch>
        
     </div>
     </>
     )}
     </Router>
    </div>
   
  );
}

export default App;
