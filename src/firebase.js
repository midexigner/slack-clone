import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyC9cGoZ6xuon9s5jkjUiS5AAUicXGahB3Q",
  authDomain: "slack-clone-fe378.firebaseapp.com",
  databaseURL: "https://slack-clone-fe378.firebaseio.com",
  projectId: "slack-clone-fe378",
  storageBucket: "slack-clone-fe378.appspot.com",
  messagingSenderId: "109003342735",
  appId: "1:109003342735:web:08768edbbb30721e3301fa"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider(); 

export { auth, provider };
export default db;